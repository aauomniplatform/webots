#include <webots/DistanceSensor.hpp>
#include <webots/Motor.hpp>
#include <webots/Robot.hpp>
#include <webots/Camera.hpp>


#define TIME_STEP 64
using namespace webots;

int main(int argc, char **argv) {
  Robot *robot = new Robot();

 Camera *camera[1]; // change to 2 for 2 cameras
  char cameraNames[4][11] = {"camera lh8"}; // "camera2"}; //- add this for multisense but doesnt work since something needs to be declared i.e. right and left camera
  for (int i = 0; i < 1; i++) {   //i<2 for 2
    camera[i] = robot->getCamera(cameraNames[i]);
    camera[i]->enable(TIME_STEP);
 }

  Motor *wheels[4];
  char wheels_names[4][8] = {"wheel1", "wheel2", "wheel3", "wheel4"};
  for (int i = 0; i < 4; i++) {
    wheels[i] = robot->getMotor(wheels_names[i]);
    wheels[i]->setPosition(INFINITY);
    wheels[i]->setVelocity(0.0);
  }
  
  int avoidObstacleCounter = 0;
  while (robot->step(TIME_STEP) != -1) {
    double leftSpeed = 3.0;
    double rightSpeed = 3.0;
    if (avoidObstacleCounter > 0) {
      avoidObstacleCounter--;
      leftSpeed = -1.0;
      rightSpeed = -1.0;
    } else { // read sensors
      for (int i = 0; i < 4; i++) {
   //     if (ds[i]->getValue() < 9500.0)
          avoidObstacleCounter = 100;
      }
    }
 
    wheels[0]->setVelocity(leftSpeed);
    wheels[1]->setVelocity(rightSpeed);
    wheels[2]->setVelocity(leftSpeed);
    wheels[3]->setVelocity(rightSpeed);
  }
  delete robot;
  return 0;  // EXIT_SUCCESS
}