"""gps_get_py controller."""
# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot
from controller import GPS
from controller import Compass
from controller import Motor
from numpy import linalg as LA
import math
import numpy as np
from collections import deque

def clamp(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n
        
        
def controller_wheel(timestep,current_pose,Destination_pose,X):
    #######initilizations
    
    m =200
    ly =0.8
    lx = 0.5
    r = 0.127
    max_linear_speed = 12
    max_angular_speed = 4
    
    #######inverse kinematic Equations
    Pi = current_pose
    
    B = np.array([[1, 1, 1, 1],[-1, 1, 1, -1 ],[-1/(lx+ly), 1/(lx+ly), -1/(lx+ly), 1/(lx+ly)]])
    B_inv = np.array([[1, -1, -(lx+ly)/2],[1, 1, (lx+ly)/2] ,[1, 1, -(lx+ly)/2], [1, -1, (lx+ly)/2]])
    
    
    # X = Kp*np.subtract(Destination_pose,Pi)/dt
    
    X[0]=clamp(X[0],-max_linear_speed,max_linear_speed)
    X[1]=clamp(X[1],-max_linear_speed,max_linear_speed)
    X[2]=clamp(X[2],-max_angular_speed,max_angular_speed)
    
    R = np.array([[math.cos(Pi[2]), -math.sin(Pi[2]), 0],[math.sin(Pi[2]), math.cos(Pi[2]), 0],[ 0, 0, 1]])

    U_inv =X*(1/r)
    
    X_next_inv = np.dot(np.dot(B_inv,np.linalg.inv(R)),U_inv)
    
    
    #wheel velocities 
    U =X_next_inv*(r/4)
    
    return U

# create the Robot instance.
robot = Robot()

timestep = int(robot.getBasicTimeStep())

gps = robot.getGPS('gps_lh8')
compass = robot.getCompass('compass_lh8')

top_right_wheel = robot.getMotor("wheel1")
top_left_wheel = robot.getMotor("wheel2")
bottom_left_wheel = robot.getMotor("wheel3")
bottom_right_wheel = robot.getMotor("wheel4")

top_right_wheel.setPosition(float('+inf'))
top_left_wheel.setPosition(float('+inf'))
bottom_left_wheel.setPosition(float('+inf'))
bottom_right_wheel.setPosition(float('+inf'))
  

gps.enable(timestep)
compass.enable(timestep)

ze = np.zeros([3,1])
Int = deque()
Int.appendleft(ze)
# Int.appendleft(ze)
# Int.appendleft(ze)
# Int.appendleft(ze)
# Int.appendleft(ze)

##destination points
x = 2
z = 2
th = 0
destination = np.array([x,z])
Destination_pose = np.array([[x],[z],[th]])
prev_err=0

#gains
Kp= 0.5
Kd=0.01
Ki=0.01


while robot.step(timestep) != -1:
    position = gps.getValues()
    orientation = compass.getValues()
    
    ####position and orientaion
    v_gps = np.array([position[0],position[2]])
    v_front = np.array([orientation[0],-orientation[2]])
    v_right = np.array([-v_front[1],v_front[0]])
    v_north = np.array([1,0])
    
    v_dir = np.subtract(destination,v_gps)
    
    distance = LA.norm(v_dir)
    
    theta =  -math.atan2(v_north[1],v_north[0]) + math.atan2(v_front[1],v_front[0])
    print(theta)
    delta_theta = theta - th
    
    ####current pose
    current_pose = np.array([[v_gps[0]],[v_gps[1]],[theta]])
    
    ##PID
    dt = timestep*0.001
    Error = Destination_pose - current_pose
    
    Prop = Error
    Der  = (Error - prev_err)/dt
    Int.pop()
    Int.appendleft((Error + prev_err)*dt/2)
    I = np.sum(Int,axis=1)
    
    PID  = Kp*Prop + Ki*I+ Kd*Der
    
    prev_err = Error
    
    X = PID/dt
    
    #######controller
    motor_vel = controller_wheel(timestep,current_pose,Destination_pose,X)

    
    top_right_wheel.setVelocity(float(motor_vel[0]));
    top_left_wheel.setVelocity(float(motor_vel[1]));
    bottom_left_wheel.setVelocity(float(motor_vel[2]));
    bottom_right_wheel.setVelocity(float(motor_vel[3]));
  
    pass

# Enter here exit cleanup code.

