#include <webots/DistanceSensor.hpp>
#include <webots/Motor.hpp>
#include <webots/Robot.hpp>
#include <webots/Camera.hpp>


#define TIME_STEP 64
using namespace webots;

int main(int argc, char **argv) {
  Robot *robot = new Robot();

  Motor *wheels[4];
  char wheels_names[4][8] = {"wheel1", "wheel2", "wheel3", "wheel4"};
  for (int i = 0; i < 4; i++) {
    wheels[i] = robot->getMotor(wheels_names[i]);
    wheels[i]->setPosition(INFINITY);
    wheels[i]->setVelocity(0.0);
  }
  
  int avoidObstacleCounter = 0;
  while (robot->step(TIME_STEP) != -1) {
    double leftSpeed = 7.0;
    double rightSpeed = 7.0;
    if (avoidObstacleCounter > 0) {
      avoidObstacleCounter--;
      leftSpeed = 5.0;
      rightSpeed = 5.0;
    } else { // read sensors
      for (int i = 0; i < 4; i++) {
   //     if (ds[i]->getValue() < 9500.0)
          avoidObstacleCounter = 100;
      }
    }
 
   wheels[0]->setVelocity(rightSpeed);
   wheels[1]->setVelocity(leftSpeed);
   wheels[2]->setVelocity(leftSpeed);
   wheels[3]->setVelocity(rightSpeed);
  }
  delete robot;
  return 0;  // EXIT_SUCCESS
}