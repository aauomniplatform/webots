#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <webots/lidar.h>
#include <webots/motor.h>
#include <webots/robot.h>

#define TIME_STEP 32
#define MAX_SPEED 6.4
#define CRUISING_SPEED 5.0
#define OBSTACLE_THRESHOLD 0.1
#define DECREASE_FACTOR 0.9
#define BACK_SLOWDOWN 0.9

// gaussian function
double gaussian(double x, double mu, double sigma) {
  return (1.0 / (sigma * sqrt(2.0 * M_PI))) * exp(-((x - mu) * (x - mu)) / (2 * sigma * sigma));
}

int main(int argc, char **argv) {
  // init webots stuff
  wb_robot_init();

  // get devices
  WbDeviceTag lh8 = wb_robot_get_device("LDS_2" );
  WbDeviceTag lh8s = wb_robot_get_device("LDS_1" );
  WbDeviceTag front_left_wheel = wb_robot_get_device("wheel1");
  WbDeviceTag front_right_wheel = wb_robot_get_device("wheel2");
  WbDeviceTag back_left_wheel = wb_robot_get_device("wheel3");
  WbDeviceTag back_right_wheel = wb_robot_get_device("wheel4");

  // init lh8
  wb_lidar_enable(lh8, TIME_STEP);
  const int lh8_width = wb_lidar_get_horizontal_resolution(lh8);
  const int half_width = lh8_width / 2;
  const int max_range = wb_lidar_get_max_range(lh8);
  const double range_threshold = max_range / 20.0;
  const float *lh8_values = NULL;
  
  wb_lidar_enable(lh8s, TIME_STEP);
  const int lh8s_width = wb_lidar_get_horizontal_resolution(lh8s);
  const int halfs_width = lh8s_width / 2;
  const int maxs_range = wb_lidar_get_max_range(lh8s);
  const double ranges_threshold = max_range / 20.0;
  const float *lh8s_values = NULL;


  // init braitenberg coefficient
  double *const braitenberg_coefficients = (double *)malloc(sizeof(double) * lh8_width);
  int i, j;
  for (i = 0; i < lh8_width; ++i)
    braitenberg_coefficients[i] = gaussian(i, half_width, lh8_width / 5);
    
  double *const braitenberg_coefficientss = (double *)malloc(sizeof(double) * lh8s_width);
  int k, l;
  for (i = 0; i < lh8s_width; ++i)
    braitenberg_coefficientss[i] = gaussian(i, halfs_width, lh8s_width / 5);

  // init motors
  wb_motor_set_position(front_left_wheel, INFINITY);
  wb_motor_set_position(front_right_wheel, INFINITY);
  wb_motor_set_position(back_left_wheel, INFINITY);
  wb_motor_set_position(back_right_wheel, INFINITY);

  // init speed for each wheel
  double back_left_speed = 0.0, back_right_speed = 0.0;
  double front_left_speed = 0.0, front_right_speed = 0.0;
  wb_motor_set_velocity(front_left_wheel, front_left_speed);
  wb_motor_set_velocity(front_right_wheel, front_right_speed);
  wb_motor_set_velocity(back_left_wheel, back_left_speed);
  wb_motor_set_velocity(back_right_wheel, back_right_speed);

  // init dynamic variables
  double left_obstacle = 0.0, right_obstacle = 0.0;

  // control loop
  while (wb_robot_step(TIME_STEP) != -1) {
    // get lidar values
    lh8_values = wb_lidar_get_range_image(lh8);
    // apply the braitenberg coefficients on the resulted values of the lh8
    // near obstacle sensed on the left side
    for (i = 0; i < half_width; ++i) {
      if (lh8_values[i] < range_threshold)  // far obstacles are ignored
        left_obstacle += braitenberg_coefficients[i] * (1.0 - lh8_values[i] / max_range);
      // near obstacle sensed on the right side
      j = lh8_width - i - 1;
      if (lh8_values[j] < range_threshold)
        right_obstacle += braitenberg_coefficients[i] * (1.0 - lh8_values[j] / max_range);
    }
    // overall front obstacle
    const double obstacle = left_obstacle + right_obstacle;
    // compute the speed according to the information on
    // obstacles
    if (obstacle > OBSTACLE_THRESHOLD) {
      const double speed_factor = (1.0 - DECREASE_FACTOR * obstacle) * MAX_SPEED / obstacle;
      front_left_speed = speed_factor * left_obstacle;
      front_right_speed = speed_factor * right_obstacle;
      back_left_speed = BACK_SLOWDOWN * front_left_speed;
      back_right_speed = BACK_SLOWDOWN * front_right_speed;
    } else {
      back_left_speed = CRUISING_SPEED;
      back_right_speed = CRUISING_SPEED;
      front_left_speed = CRUISING_SPEED;
      front_right_speed = CRUISING_SPEED;
    }
    
    
      while (wb_robot_step(TIME_STEP) != -1) {
    // get lidar values
    lh8s_values = wb_lidar_get_range_image(lh8s);
    // apply the braitenberg coefficients on the resulted values of the lh8
    // near obstacle sensed on the left side
    for (k = 0; k < half_width; ++k) {
      if (lh8s_values[i] < range_threshold)  // far obstacles are ignored
        left_obstacle += braitenberg_coefficientss[k] * (1.0 - lh8s_values[k] / maxs_range);
      // near obstacle sensed on the right side
      l = lh8s_width - k - 1;
      if (lh8s_values[j] < ranges_threshold)
        right_obstacle += braitenberg_coefficientss[k] * (1.0 - lh8s_values[l] / maxs_range);
    }
    // overall front obstacle
    const double obstacle = left_obstacle + right_obstacle;
    // compute the speed according to the information on
    // obstacles
    if (obstacle > OBSTACLE_THRESHOLD) {
      const double speed_factor = (1.0 - DECREASE_FACTOR * obstacle) * MAX_SPEED / obstacle;
      front_left_speed = speed_factor * left_obstacle;
      front_right_speed = speed_factor * right_obstacle;
      back_left_speed = BACK_SLOWDOWN * front_left_speed;
      back_right_speed = BACK_SLOWDOWN * front_right_speed;
    } else {
      back_left_speed = CRUISING_SPEED;
      back_right_speed = CRUISING_SPEED;
      front_left_speed = CRUISING_SPEED;
      front_right_speed = CRUISING_SPEED;
    }
    
    // set actuators
    wb_motor_set_velocity(front_left_wheel, front_left_speed);
    wb_motor_set_velocity(front_right_wheel, front_right_speed);
    wb_motor_set_velocity(back_left_wheel, back_left_speed);
    wb_motor_set_velocity(back_right_wheel, back_right_speed);

    // reset dynamic variables to zero
    left_obstacle = 0.0;
    right_obstacle = 0.0;
 

  free(braitenberg_coefficients);
  wb_robot_cleanup();

  return 0;
}
}
}

