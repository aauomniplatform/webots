
#include <webots/keyboard.h>
#include <webots/robot.h>
#include <webots/motor.h>
#include <webots/gps.h>

#include <arm.h>
#include <base.h>
#include <gripper.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define TIME_STEP 32


static void step() {
  if (wb_robot_step(TIME_STEP) == -1) {
    wb_robot_cleanup();
    exit(EXIT_SUCCESS);
  }
}

static void passive_wait(double sec) {
  double start_time = wb_robot_get_time();
  do {
    step();
  } while (start_time + sec > wb_robot_get_time());
}

int main(int argc, char **argv) {
  wb_robot_init();

  base_init();
  // arm_init();
  // gripper_init();
  // passive_wait(2.0);

  // int pc = 0;
  wb_keyboard_enable(TIME_STEP);
  double x=10, z=10,theta=0;
  base_goto_init(TIME_STEP);
  base_goto_set_target(x,z,theta);

  while (true) {
    step();
    base_goto_run();
  }

  wb_robot_cleanup();

  return 0;
}
