#include <webots/robot.h>
#include <webots/gps.h>
#include <stdio.h>

static WbDeviceTag gps;


int main() {
  wb_robot_init();
  
  gps = wb_robot_get_device("gps");
  if (gps)
    wb_gps_enable(gps, time_step);
    
  const double *gps_raw_values = wb_gps_get_values(gps);
  
  
  while(wb_robot_step(32) != -1)
     printf("gps [%d %d %d]\n", gps_raw_values[0],gps_raw_values[1],gps_raw_values[2]);


  wb_robot_cleanup();

  return 0;
}