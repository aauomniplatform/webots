#include <webots/Robot.hpp>
#include <webots/Motor.hpp>
#include <webots/GPS.hpp>
#include <webots/Compass.hpp>
#include <webots/InertialUnit.hpp>
#include <iostream>
#include <cmath>
#include <stdio.h>
#include <vector>
#define TIME_STEP 64
#define MAX_SPEED 6.28

// Motor *wheels[];

using namespace webots;
using namespace std;

int main(int argc, char **argv) {
  Robot *robot = new Robot();

  int timeStep = (int) robot->getBasicTimeStep();
  GPS *gps = robot->getGPS("gps");
  Compass *compass = robot->getCompass("compass");
  InertialUnit *imu = robot->getInertialUnit("inertial unit");
  gps->enable(timeStep);
  imu->enable(timeStep);
  compass->enable(timeStep);
  
  Motor *top_right_wheel = robot->getMotor("wheel1");
  Motor *top_left_wheel = robot->getMotor("wheel2");
  Motor *bottom_left_wheel = robot->getMotor("wheel3");
  Motor *bottom_right_wheel = robot->getMotor("wheel4");
  
  top_right_wheel->setPosition(INFINITY);
  top_left_wheel->setPosition(INFINITY);
  bottom_left_wheel->setPosition(INFINITY);
  bottom_right_wheel->setPosition(INFINITY);
  
  top_right_wheel->setVelocity(0.1 * MAX_SPEED);
  top_left_wheel->setVelocity(0.1 * MAX_SPEED);
  bottom_left_wheel->setVelocity(0.1 * MAX_SPEED);
  bottom_right_wheel->setVelocity(0.1 * MAX_SPEED);
  
  // %%
// %initial conditions
// %%%%%%%%%%%%%%%%%%%%%%%%%%
  float tmax= 10;
// dt = 0.1;
  float n= round(tmax/(0.001*TIME_STEP));
  float mass =22;
  float ly =0.56;
  float lx = 0.23;
  float W0 = 0;
  float W1 = 0;
  float W2 = 0;
  float W3 = 0;
  float Vx=0;
  float Vy=0;
  float omega=0;
  float Px=0;
  float Py=0;
  float theta=0;
  float r = 0.051;

// %%
// %%%destination position and orientation
  float Dx= -1;
  float Dy=5;
  float Dt=0;
  // float Desired_P[] ={Dx,Dy,Dt};
  // %controller initialization
  // Prop(3,1:n+1) = 0; Der(3,1:n+1) = 0; Int(3,1:n+1) = 0; I(3,1:n+1) = 0;
  // PID(3,1:n+1) = 0;
  // FeedBack(3,1:n+1) = 0;
  // Output(3,1:n+1) = 0;
  // Error(3,1:n+1) = 0;
  
  // %PID gains(needs tuning)
  float Kp= 0.5;
  float Ki = 0.01;      //     % Integral term Ki
  float Kd = 0.01; 

// %%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%
// %inverse kinematic Equations
// %%X= B*U
// Pi = [Px; Py ;theta];
// X_inv = [W0 ; W1 ; W2; W3];
// X = [Vx ; Vy ;omega];

// B = [1 1 1 1; -1 1 1 -1 ;-1/(lx+ly) 1/(lx+ly) -1/(lx+ly) 1/(lx+ly)];
// B_inv = [1 -1 -(lx+ly)/2; 1 1 (lx+ly)/2 ;1 1 -(lx+ly)/2 ;1 -1 (lx+ly)/2 ];
// max_linear_speed = 1.414;
// max_angular_speed = 0.5;


// %%
// %main loop
// for i = 1:n
    // %%controller
    // %PID controller
    // Error(:,i+1) = Desired_P - Pi(:,end); % error entering the PID controller
    
    // Prop(:,i+1) = Error(:,i+1);% error of proportional term
    // Der(:,i+1)  = (Error(:,i+1) - Error(:,i))/dt; % derivative of the error
    // Int(:,i+1)  = (Error(:,i+1) + Error(:,i))*dt/2; % integration of the error
    // I(:,i+1)    = sum(Int,2); % the sum of the integration of the error
    
    // PID(:,i+1)  = Kp*Prop(:,i) + Ki*I(:,i+1)+ Kd*Der(:,i); % the three PID terms
    
    
    // Vx =PID(1,i+1)./dt;
    // Vy = PID(2,i+1)./dt;
    // omega = PID(3,i+1)./dt;
    // %%
     // %%max limits
    // Vx(Vx>max_linear_speed)=max_linear_speed;
    // Vx(Vx<-max_linear_speed)=-max_linear_speed;
    
    // Vy(Vy>max_linear_speed)=max_linear_speed;
    // Vy(Vy<-max_linear_speed)=-max_linear_speed;
    
    // omega(omega>max_angular_speed) = max_angular_speed;
    // omega(omega<-max_angular_speed) = -max_angular_speed;
    // %%
    // %inverse Kinematics
    
    // %rotation matrix from body to world frame
    // R = [cos(Pi(3,end)) -sin(Pi(3,end)) 0; sin(Pi(3,end)) cos(Pi(3,end)) 0; 0 0 1];
   
    // %getting bot's velocities
    // U_inv =[Vx;Vy;omega].*(1/r);
    
    // %getting wheel velocities
    // X_next_inv = B_inv*inv(R)*U_inv;
    // %%
    // %forward kinematics
    
    // %wheel velocities 
    // U =X_next_inv.*(r/4);
    
    // %robot velocities
    // X_next = R*B*U;
    
    
    // X = [X,X_next];
    
    // %robot positions
    // P = Pi(:,end) + X(:,end).*dt;
    
    // Pi = [Pi, P];

  // Main control loop
  while (robot->step(timeStep) != -1) {
    
    
    
    
  
    // set up the motor speeds at 10% of the MAX_SPEED.
    // leftMotor->setVelocity(0.1 * MAX_SPEED);
    // rightMotor->setVelocity(0.1 * MAX_SPEED);
    // Read the sensors
    // const double *gpsval = gps->getValues();
    // printf("gps [%f %f %f]\n", *gpsval, *gpsval+1, *gpsval+2);
    // const double *comval = compass->getValues();
    // printf("compass [%f %f %f]\n", *comval, *comval+1, *comval+2);
    
    const double *imu_val = imu->getRollPitchYaw();
    printf("imu val RPY [%f %f %f]\n", *imu_val, *imu_val+1, *imu_val+2);
    // cout << "gps data " << val[0] << endl;

    // Process sensor data here

    // Enter here functions to send actuator commands
    // led->set(1);
  }

  delete robot;
  return 0;
}