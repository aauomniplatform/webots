"""gps_get_py controller."""
# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot
from controller import GPS
from controller import Compass
from controller import Motor
from numpy import linalg as LA
import math
import numpy as np

SPEED = 8.0
##destination points
x = 5
z = 5
th = 0.5

def clamp(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n
        
        
def controller_wheel(position,orientation,destination):
    #gains
    K1=3
    K2=1
    K3=1
    
    v_gps = np.array([position[0],position[2]])
    v_front = np.array([orientation[0],orientation[1]])
    v_right = np.array([-v_front[1],v_front[0]])
    v_north = np.array([1,0])
    
    v_dir = np.subtract(destination,v_gps)
    
    distance = LA.norm(v_dir)
        
    theta =  math.atan2(v_north[1],v_north[0]) - math.atan2(v_front[1],v_front[0])
    delta_theta = theta - th
    
    
    transform = np.eye(3)
    transform[0,0] = v_front[0]
    transform[0,1] = v_right[0]
    transform[1,0] = v_front[1]
    transform[1,1] = v_right[1]
    transform[2,0] = -v_front[0]*v_gps[0] - v_front[1]*v_gps[1]
    transform[2,1] = -v_right[0]*v_gps[0] - v_right[1]*v_gps[1]
    v_target_tmp = np.array([destination[0],destination[1],1])
    v_target_rel = np.dot(v_target_tmp,transform)
    
    speeds = np.array([0.000,0.000,0.000,0.000])
    
    speeds[0] = -delta_theta / math.pi * K1
    speeds[1] = delta_theta / math.pi * K1
    speeds[2] = -delta_theta / math.pi * K1
    speeds[3] = delta_theta / math.pi * K1

    speeds[0] += v_target_rel[0] * K2
    speeds[1] += v_target_rel[0] * K2
    speeds[2] += v_target_rel[0] * K2
    speeds[3] += v_target_rel[0] * K2

    speeds[0] += -v_target_rel[1] * K3
    speeds[1] += v_target_rel[1] * K3
    speeds[2] += v_target_rel[1] * K3
    speeds[3] += -v_target_rel[1] * K3
    
    for i in range(0,4):
        speeds[i] /= (K1 + K2 + K2)
        speeds[i] *= SPEED
        speeds[i] *= 30.0
        speeds[i]= clamp(speeds[i],-SPEED,SPEED)
        
    return speeds

# create the Robot instance.
robot = Robot()

timestep = int(robot.getBasicTimeStep())

gps = robot.getGPS('gps_lh8')
compass = robot.getCompass('compass_lh8')

top_right_wheel = robot.getMotor("wheel1")
top_left_wheel = robot.getMotor("wheel2")
bottom_left_wheel = robot.getMotor("wheel3")
bottom_right_wheel = robot.getMotor("wheel4")

top_right_wheel.setPosition(float('+inf'))
top_left_wheel.setPosition(float('+inf'))
bottom_left_wheel.setPosition(float('+inf'))
bottom_right_wheel.setPosition(float('+inf'))
  

gps.enable(timestep)
compass.enable(timestep)



destination = np.array([x,z])
# Destination_pose = np.array([[x],[z],[th]])

while robot.step(timestep) != -1:
    ###position and orientaion
    position = gps.getValues()
    orientation = compass.getValues()
    
    
    ####controller
    
    motor_vel = controller_wheel(position,orientation,destination)

    top_right_wheel.setVelocity(float(motor_vel[0]));
    top_left_wheel.setVelocity(float(motor_vel[1]));
    bottom_left_wheel.setVelocity(float(motor_vel[2]));
    bottom_right_wheel.setVelocity(float(motor_vel[3]));
          
    pass

# Enter here exit cleanup code.
