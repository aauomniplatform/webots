This repository is for webots (and not ros).
It contains webots worlds from V1-V6.

![Webots environments from V1 - V6](/Images/Webotenv.gif)

-----------------------------------------------------

## Authors

Ditte Damgaard Albertsen:	        dalber16@student.aau.dk

Rahul Ravichandran:			rravic19@student.aau.dk

Rasmus Thomsen:				rthoms16@student.aau.dk

Mathiebhan Mahendran:			mmahen15@student.aau.dk

Alexander Staal:			astaal16@student.aau.dk

Carolina Bucle Infinito:                cgomez19@student.aau.dk

-------------------------------------------------------

YouTube - https://www.youtube.com/playlist?list=PLSG9gXgVHC2OaHaly2AfDesKk5W0MFBX2
